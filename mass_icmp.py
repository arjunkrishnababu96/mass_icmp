import subprocess

def generate_ip():
    ip = list()
    for s in range(2,75+1):
        for m in [1,2,17,18,33,34,49,50]:
            ip.append("192.168.{}.{}".format(s,m))
    return ip

def main():
    ip = generate_ip()

    for address in ip:
        params = ["iperf", "-c", address, "-t", "0.001", "-r"]
        result = subprocess.run(params,
                                encoding='utf-8',
                                stdout=subprocess.PIPE)
        outputs = result.stdout.split('\n')
        if len(outputs) > 5:
            print(outputs[4])

if __name__ == '__main__':
    main()
